From: Stefano Rivera <stefanor@debian.org>
Date: Wed, 23 Sep 2020 17:04:10 -0700
Subject: Tests: Change TLS protocol for Debian

Debian Unstable has disabled TLS 1.0 and 1.1 for SSLv23_METHOD(). Change
TLS/SSL protocol of some tests to PROTOCOL_TLS or PROTOCOL_TLSv1_2 to
make them pass on Debian.

Author: Christian Heimes <christian@python.org>
Bug-cPython: https://bugs.python.org/issue31518
Origin: cpython, https://github.com/python/cpython/pull/3661
---
 lib-python/2.7/test/test_ftplib.py           | 6 +++---
 lib-python/2.7/test/test_ssl.py              | 2 +-
 lib-python/2.7/test/test_urllib2_localnet.py | 2 +-
 3 files changed, 5 insertions(+), 5 deletions(-)

diff --git a/lib-python/2.7/test/test_ftplib.py b/lib-python/2.7/test/test_ftplib.py
index 05e2595..944cb93 100644
--- a/lib-python/2.7/test/test_ftplib.py
+++ b/lib-python/2.7/test/test_ftplib.py
@@ -707,11 +707,11 @@ class TestTLS_FTPClass(TestCase):
             self.client.auth()
             self.assertRaises(ValueError, self.client.auth)
         finally:
-            self.client.ssl_version = ssl.PROTOCOL_TLSv1
+            self.client.ssl_version = ssl.PROTOCOL_TLS
 
     def test_context(self):
         self.client.quit()
-        ctx = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
+        ctx = ssl.SSLContext(ssl.PROTOCOL_TLS)
         self.assertRaises(ValueError, ftplib.FTP_TLS, keyfile=CERTFILE,
                           context=ctx)
         self.assertRaises(ValueError, ftplib.FTP_TLS, certfile=CERTFILE,
@@ -736,7 +736,7 @@ class TestTLS_FTPClass(TestCase):
 
     def test_check_hostname(self):
         self.client.quit()
-        ctx = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
+        ctx = ssl.SSLContext(ssl.PROTOCOL_TLS)
         ctx.verify_mode = ssl.CERT_REQUIRED
         ctx.check_hostname = True
         ctx.load_verify_locations(CAFILE)
diff --git a/lib-python/2.7/test/test_ssl.py b/lib-python/2.7/test/test_ssl.py
index e46dcd8..7140924 100644
--- a/lib-python/2.7/test/test_ssl.py
+++ b/lib-python/2.7/test/test_ssl.py
@@ -1799,7 +1799,7 @@ else:
             else:
                 self.context = ssl.SSLContext(ssl_version
                                               if ssl_version is not None
-                                              else ssl.PROTOCOL_TLSv1)
+                                              else ssl.PROTOCOL_TLS)
                 self.context.verify_mode = (certreqs if certreqs is not None
                                             else ssl.CERT_NONE)
                 if cacerts:
diff --git a/lib-python/2.7/test/test_urllib2_localnet.py b/lib-python/2.7/test/test_urllib2_localnet.py
index 9199cb9..aab4052 100644
--- a/lib-python/2.7/test/test_urllib2_localnet.py
+++ b/lib-python/2.7/test/test_urllib2_localnet.py
@@ -593,7 +593,7 @@ class TestUrlopen(BaseTestCase):
         sni_name = [None]
         def cb_sni(ssl_sock, server_name, initial_context):
             sni_name[0] = server_name
-        context = ssl.SSLContext(ssl.PROTOCOL_TLSv1)
+        context = ssl.SSLContext(ssl.PROTOCOL_TLS)
         context.set_servername_callback(cb_sni)
         handler = self.start_https_server(context=context, certfile=CERT_localhost)
         context = ssl.create_default_context(cafile=CERT_localhost)
