============================
What's new in PyPy2.7 7.3.2+
============================

.. this is a revision shortly after release-pypy-7.3.2
.. startrev: c136fdb316e4


.. branch: cross_compilation_fixes

Respect PKG_CONFIG and CC in more places to allow cross-compilation
